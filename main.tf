terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "${var.region}"
}

resource "aws_s3_bucket" "meu-bucket" {
  bucket = "lukazprs-gitlab01"

  tags = {
    Name        = "terraform-cloud gitlab"
    Environment = "Test gitlab"
  }
}

resource "aws_s3_bucket_acl" "minha-acl" {
  bucket = aws_s3_bucket.meu-bucket.id
  acl    = "private"
}
